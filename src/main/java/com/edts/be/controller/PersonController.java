package com.edts.be.controller;

import com.edts.be.model.Person;
import com.edts.be.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class PersonController {
    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping(value = "/get-all-person")
    public Iterable<Person> getAllPerson() {
        return personService.getAllPerson();
    }

    @GetMapping(value = "/edit-person")
    public Person editPerson(@RequestParam long id, @RequestParam String name, @RequestParam String about, @RequestParam int birthYear) {
        return personService.editPerson(id, name, about, birthYear);
    }

    @PutMapping(value = "/edit-person")
    public Person editPerson(@RequestBody Person person) {
        try {
            return personService.editPersonWithPost(person);
        } catch (Exception ex) {
            System.out.println("error edit person : " + ex.getMessage());
            return null;
        }
    }

    @PostMapping(value = "/add-person")
    public Person addPerson(@RequestBody Person person) {
        if (person.getId() != 0) {
            System.out.println("id tidak boleh ada isi");
            return null;
        }
        try {
            return personService.addPersonWithPost(person);
        } catch (Exception ex) {
            System.out.println("error add person : " + ex.getMessage());
            return null;
        }
    }
}
