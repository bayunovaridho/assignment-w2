package com.edts.be.controller;

import com.edts.be.bean.EditBean;
import com.edts.be.bean.LoginBean;
import com.edts.be.bean.LogoutBean;
import com.edts.be.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController {
    private final AuthenticationService authenticationService;

    @Autowired
    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping(value = "/login")
    public String login(@RequestBody LoginBean loginBean) {
        return authenticationService.login(loginBean);
    }

    @PostMapping(value = "/logout")
    public String logout(@RequestBody LogoutBean logoutBean) {
        return authenticationService.logout(logoutBean);
    }

    @PostMapping(value = "/edit-authentication")
    public String edit(@RequestBody EditBean editBean) {
        return authenticationService.edit(editBean);
    }
}

//    @PostMapping(value = "/login")
//    public String login(@RequestParam long personId, String password) {
//        LoginBean data = new LoginBean();
//        data.setPersonId(personId);
//        data.setPassword(password);
//        return authenticationService.login(data);
//    }
//
//    @PostMapping(value = "/logout")
//    public String logout(@RequestParam String sessionId, String personId) {
//        return authenticationService.logout(sessionId, personId);
//    }
//}
