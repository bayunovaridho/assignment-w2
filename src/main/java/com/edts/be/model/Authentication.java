package com.edts.be.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Authentication {
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) long id;
    private long personId;
    private String sessionId; //UUID.randomUUID().toString()
    private Date loginDate;
    private Date logoutDate;

    //Constructor
    public Authentication() {
    }

    public Authentication(long id, long personId, String sessionId, Date loginDate, Date logoutDate) {
        this.id = id;
        this.personId = personId;
        this.sessionId = sessionId;
        this.loginDate = loginDate;
        this.logoutDate = logoutDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPersonId() {
        return personId;
    }

    public void setPersonId(long personId) {
        this.personId = personId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public Date getLogoutDate() {
        return logoutDate;
    }

    public void setLogoutDate(Date logoutDate) {
        this.logoutDate = logoutDate;
    }
}