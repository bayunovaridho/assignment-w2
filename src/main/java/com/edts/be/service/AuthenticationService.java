package com.edts.be.service;

import com.edts.be.bean.EditBean;
import com.edts.be.bean.LoginBean;
import com.edts.be.bean.LogoutBean;
import com.edts.be.model.Authentication;
import com.edts.be.model.Person;
import com.edts.be.repository.AuthenticationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
public class AuthenticationService {
    private final AuthenticationRepository authenticationRepository;
    private final PersonService personService;

    @Autowired
    public AuthenticationService(AuthenticationRepository authenticationRepository, PersonService personService) {
        this.authenticationRepository = authenticationRepository;
        this.personService = personService;
    }

    //login
    public String login(LoginBean data) {
        Optional<Person> personLogin = personService.getById(data.getPersonId());
        if (personLogin.isEmpty()) {
            return "" + data.getPersonId() + "doesn't exist";
        }

        if (!data.getPassword().equals(personLogin.get().getPassword())) {
            return "password doesnt match";
        }
        String sessionId = generateSessionId();

        Authentication authenticationSave = new Authentication();
        authenticationSave.setSessionId(sessionId);
        authenticationSave.setPersonId(personLogin.get().getId());
        authenticationSave.setLoginDate(new Date());
        authenticationRepository.save(authenticationSave);
        return "your session id " + sessionId;
    }

    //logout
    public String logout(LogoutBean logoutBean) {
        Optional<Authentication> authOptional = authenticationRepository.findBySessionIdAndPersonId(logoutBean.getSessionId(), logoutBean.getPersonId());
        if (authOptional.isEmpty()) {
            return "session not found";
        }

        Authentication authentication = authOptional.get();
        authentication.setLogoutDate(new Date());
        authenticationRepository.save(authentication);
        return "logout success";
    }

    //edit
    public String edit(EditBean editBean) {
        Optional<Authentication> authOptional2 = authenticationRepository.findBySessionIdAndPersonId(editBean.getSessionId(), editBean.getPersonId());
        if (authOptional2.isEmpty()) {
            return "unauthorized";
        }
//        editBean.setAbout(editBean.getAbout());
//        editBean.setBirthYear(editBean.getBirthYear());
        Optional<Person> editAuth = personService.getById(editBean.getPersonId());
        if (editAuth.isEmpty()){
            return "tidak ditemukan";
        }
        Person person2 = editAuth.get();
        person2.setAbout(editBean.getAbout());
        person2.setBirthYear(editBean.getBirthYear());
        personService.saveEditing(person2);
        return "edit success";
    }

    private String generateSessionId() {
        return UUID.randomUUID().toString();
    }
}