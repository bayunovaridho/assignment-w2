package com.edts.be.service;

import com.edts.be.model.Person;
import com.edts.be.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PersonService {

    private final PersonRepository personRepository;

    @Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Iterable<Person> getAllPerson() {
        return personRepository.findAll();
    }

    public Person editPerson(long id, String name, String about, int birthYear) {
        Optional<Person> personOptional = personRepository.findById(id);
        if (personOptional.isPresent()) {
            Person person = personOptional.get();
            person.setName(name);
            person.setAbout(about);
            person.setBirthYear(birthYear);
            personRepository.save(person);
            return person;
        }
        return null;
    }

    public Person editPersonWithPost(Person person) throws Exception {
        Optional<Person> personOptional = personRepository.findById(person.getId());
        if (personOptional.isPresent()) {
            Person person1 = personOptional.get();
            person1.setAbout(person.getAbout());
            return personRepository.save(person1);
        } else {
            throw new Exception("data tidak ditemukan");
        }
    }

    public Person addPersonWithPost(Person person) {
        return personRepository.save(person);
    }
    public Optional<Person> getById(long personId){
        return personRepository.findById(personId);
    }
    public  Person saveEditing(Person editing){
        return personRepository.save(editing);
    }
}
