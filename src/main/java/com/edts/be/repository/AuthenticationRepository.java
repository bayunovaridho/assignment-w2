package com.edts.be.repository;

import com.edts.be.model.Authentication;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AuthenticationRepository extends CrudRepository<Authentication, Long> {
    Optional<Authentication> findBySessionIdAndPersonId(String sessionId, long personId);
}
